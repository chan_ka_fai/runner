package com.cjh.runner.web.form;

import org.hibernate.validator.constraints.NotEmpty;

public class OrderForTakeForm {

    @NotEmpty(message = "取货地址不能为空")
    private String PickupAddress;

    @NotEmpty(message = "收货人姓名不能为空")
    private String Consignee;

    @NotEmpty(message = "收货人联系电话不能为空")
    private String ConsigneeMobile;

    @NotEmpty(message = "取货商品不能为空")
    private String Goods;

    public String getPickupAddress() {
        return PickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        PickupAddress = pickupAddress;
    }

    public String getConsignee() {
        return Consignee;
    }

    public void setConsignee(String consignee) {
        Consignee = consignee;
    }

    public String getConsigneeMobile() {
        return ConsigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        ConsigneeMobile = consigneeMobile;
    }

    public String getGoods() {
        return Goods;
    }

    public void setGoods(String goods) {
        Goods = goods;
    }
}
