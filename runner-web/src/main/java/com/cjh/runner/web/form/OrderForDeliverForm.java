package com.cjh.runner.web.form;

import org.hibernate.validator.constraints.NotEmpty;

public class OrderForDeliverForm {

    @NotEmpty(message = "送货人姓名不能为空")
    private String deliver;

    @NotEmpty(message = "送货人联系电话不能为空")
    private String deliverMobile;

    @NotEmpty(message = "送货物品不能为空")
    private String goods;

    @NotEmpty(message = "收货人姓名不能为空")
    private String consignee;

    @NotEmpty(message = "收货人联系电话不能为空")
    private String consigneeMobile;

    public String getDeliver() {
        return deliver;
    }

    public void setDeliver(String deliver) {
        this.deliver = deliver;
    }

    public String getDeliverMobile() {
        return deliverMobile;
    }

    public void setDeliverMobile(String deliverMobile) {
        this.deliverMobile = deliverMobile;
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }
}
