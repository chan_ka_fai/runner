package com.cjh.runner.web.controller;

/*
* 公开的控制器，所有不需要身份认证的方法都会放在这个控制器中
*
*/

import com.cjh.runner.core.domain.Customer;
import com.cjh.runner.core.service.CustomerService;
import com.cjh.runner.web.form.LoginForm;
import com.cjh.runner.web.form.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/public")
public class PublicController {

    @Autowired
    public CustomerService customerService;

    //进入注册页面
    @GetMapping("/register")
    public String register() {
        return "public/register";
    }

    //执行注册操作
    @PostMapping("/register")
    public String register(@Validated RegisterForm registerForm,
                           BindingResult bindingResult,
                           Model model) {

        //验证提交的时候，有两种方式，一种前段验证，通过js验证；第二种通过后端验证
        //后端验证
//        if (username == null || "".equals(username)){
//            model.addAttribute("errorMsg", "用户账号不能为空");
//            return "publiz/register";
//        }
//        if (password == null || "".equals(password)){
//            model.addAttribute("errorMsg", "用户密码不能为空");
//            return "publiz/register";
//        }
//        if (mobile == null || "".equals(mobile)){
//            model.addAttribute("errorMsg", "手机号码不能为空");
//            return "publiz/register";
//        }
//        if (nickname == null || "".equals(nickname)){
//            model.addAttribute("errorMsg", "用户昵称不能为空");
//            return "publiz/register";
//        }
        //

        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMsg", getErrorMessage(bindingResult));
            return "public/register";
        }
        //String username,String password,String nickname,String mobile
        Customer customer = new Customer(
                registerForm.getUsername(),
                registerForm.getNickname(),
                registerForm.getPassword(),
                registerForm.getMobile());

        Customer result = customerService.register(customer);
        if (result != null) {
            //注册成功,跳转到登录界面，并携带相关提示信息

            return "public/regsuccess";
        } else {
            //注册失败
            return null;
        }

    }

    private String getErrorMessage(BindingResult bindingResult){
        StringBuilder errorMsg = new StringBuilder();
        int i = 0 ;
        for(ObjectError error : bindingResult.getAllErrors()){
            if(i !=0){
                errorMsg.append(error.getDefaultMessage() + "<br/>");
            }
            errorMsg.append(error.getDefaultMessage());
            i++;
        }
        return errorMsg.toString();
    }

    //进入登录界面
    @GetMapping("/login")
    public String login() {
        return "public/login";
    }

    //执行登录操作
    @PostMapping("/login")
    public String login(@Validated LoginForm loginForm, BindingResult bindingResult,
                        Model model, HttpSession session) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMsg", getErrorMessage(bindingResult));
            return "public/login";
        }
        Customer customer = customerService.login(loginForm.getUsername(), loginForm.getPassword());
        if (customer == null) {
            //登录失败，重新跳转到登录界面，并给出相关提示
            model.addAttribute("errorMsg", "用户名或密码不正确，请重新输入");
            return "public/login";
        } else {
            //登录成功,有1个步骤要执行

            //将用户信息存放到session
            session.setAttribute("customer", customer);

            //重定向到订单的首页
            return "redirect:/order/index";
        }
    }


    /**
     * 退出登录
     * @return
     */
    @GetMapping("/logout")
    public String logout(HttpSession session){
        session.removeAttribute("customer");
        //重定向到订单的首页
        return "redirect:/public/login";
    }

}
