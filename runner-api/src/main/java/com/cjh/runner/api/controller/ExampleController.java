package com.cjh.runner.api.controller;

import com.cjh.runner.api.utils.ResultGenerator;
import com.cjh.runner.api.vo.Result;
import com.cjh.runner.core.domain.Example;
import com.cjh.runner.core.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/example")
public class ExampleController {

    @Autowired
    private ExampleService exampleService;

    //http://localhost:8088/api/example/save?title=aa&remark=bb
    @PostMapping("/save")
    public Result save(@RequestParam String title, @RequestParam String remark){
        Example example = new Example();
        example.setTitle(title);
        example.setRemark(remark);

        Example result = exampleService.save(example);
        return ResultGenerator.ok(result);
    }

    //http://localhost:8088/api/example/{id}
    @GetMapping("/{id}")
    public Result get(@PathVariable Long Id){
        Example example = exampleService.findById(Id);
        return ResultGenerator.ok(example);

    }
}
