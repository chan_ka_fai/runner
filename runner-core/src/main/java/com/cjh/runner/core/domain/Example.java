package com.cjh.runner.core.domain;

/*
*示例的实体类
* */

import javax.persistence.*;

@Entity
@Table(name = "at_example")
public class Example {
    @Id
    @GeneratedValue
    private Long Id;//Id主键，自增长

    @Column(length = 50)
    private String title;

    private String remark;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

