package com.cjh.runner.core.repository;

import com.cjh.runner.core.domain.Customer;
import com.cjh.runner.core.domain.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Page<Order> findByCreator(Customer creator, Pageable pageable);

    Page<Order> findByDesignee(Customer distributor, Pageable pageable);
}
