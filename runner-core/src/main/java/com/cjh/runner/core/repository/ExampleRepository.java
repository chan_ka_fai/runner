package com.cjh.runner.core.repository;

import com.cjh.runner.core.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
/*
Example数据访问接口
 */
public interface ExampleRepository extends JpaRepository<Example,Long>{
}
